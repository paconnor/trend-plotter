#include <cstdlib>

#include <iostream>
#include <tuple>

#include <TFile.h>
#include <TGraph.h>
#include <TH1.h>
#include <TString.h>
#include <TKey.h>

#include "Trend.h"

using namespace std;

static const char * bold = "\e[1m", * normal = "\e[0m";

int main (int argc, char * argv[])
{
    Trend::CMS = "#scale[1.5]{#bf{CMS}}"; 

    if (argc < 4) {
        cerr << argv[0] << " firstRun lastRun lines.json\n";
        return EXIT_FAILURE;
    }

    const Run2Lumi GetLumi("deliveredLumi.txt", atoi(argv[1]), atoi(argv[2]));

    fs::path json = argv[3];
    assert(fs::exists(json));
    pt::ptree lines;
    pt::read_json(json.c_str(), lines);

    Trend::lumi = "13 TeV";

    vector<tuple<const char *, const char *, Color_t, Style_t>> alignments {
        {"prompt", "Alignment during data-taking" , kBlue    , kFullSquare      },
        {"EOY"   , "End-of-year re-reconstruction", kRed     , kFullCircle      },
        {"rereco", "Legacy reprocessing"          , kGreen+2 , kFullTriangleDown}
    };

    Trend trend("barycentre_BPIX_Y", "Barycentre of BPIX along global y coordinate",
            "Y  [#mum]", -1400, -500, lines, GetLumi);
    trend.lgd.SetHeader("BPIX barycentre");

    for (auto& alignment: alignments) {

        auto fn = Form("BPIX_Y_perRun%s.root", get<0>(alignment));
        auto f = TFile::Open(fn);

        bool earlyStop = TString(get<0>(alignment)) == "EOY";

        vector<double> run, pos;
        TIter Next(f->GetListOfKeys()) ;
        TKey* key = nullptr;
        while ( (key = dynamic_cast<TKey*>(Next())) ) {
            auto g = dynamic_cast<TGraph*>(key->ReadObj());
            assert(g != nullptr);
            auto X = g->GetPointX(0),
                 Y = g->GetPointY(0);
            if (earlyStop && X >= 320606) {
                cout << "Stopping trend at " << X << endl;
                break;
            }
            run.push_back(X);
            pos.push_back(Y);
        }

        auto g = new TGraph(run.size(), run.data(), pos.data());
        g->SetTitle      (get<1>(alignment));
        g->SetLineColor  (get<2>(alignment));
        g->SetMarkerColor(get<2>(alignment));
        //g->SetMarkerStyle(get<3>(alignment)); // TODO?

        trend(g, "PZ", "l", !earlyStop);

        f->Close();
    }

    cout << bold << "Done" << normal << endl;
    return EXIT_SUCCESS;
}
