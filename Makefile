ROOTINC    = $(shell root-config --incdir)
ROOTCFLAGS = $(shell root-config --cflags)
ROOTLIBS   = $(shell root-config --libs)

BOOSTINC = -I/usr/include/boost
BOOSTLIBS = -L/usr/lib/x86_64-linux-gnu -lboost_filesystem
#-lboost_program_options

CFLAGS=-g -Wall -O3 -std=c++1z -fPIC -DDEBUG

all: Overlap Zmm Barycentre DMRs APUs VertexSplit PrimaryVertex libTrend.so

view: all.pdf

all.pdf: run

run: all
	./APUs 272930 325176 lines.json
	./VertexSplit 272930 325176 lines.json
	./PrimaryVertex 272930 325176 lines.json
	./DMRs 272930 325176 lines.json
	./Barycentre 272930 325176 lines.json
	./Zmm 272930 325176 lines.json
	./Overlap 272930 325176 lines.json
	@rm -f all.pdf
	pdfunite *pdf all.pdf
	evince all.pdf

lib%.so: %.h %.cc 
	g++ ${CFLAGS} $(ROOTCFLAGS) -shared $^ $(ROOTLIBS) -o $@

%: %.cc
	g++ $(CFLAGS) $(ROOTCFLAGS) $^ $(ROOTLIBS) $(BOOSTLIBS) -o $@ -L. -Wl,-rpath,. -lTrend -lrt

clean: 
	@rm -f *.pdf Overlap Zmm Barycentre DMRs APUs VertexSplit PrimaryVertex *.so

.PHONY: all clean
