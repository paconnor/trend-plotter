#include <cstdlib>

#include <iostream>
#include <tuple>

#include <TFile.h>
#include <TGraph.h>
#include <TString.h>

#include "Trend.h"

using namespace std;

static const char * bold = "\e[1m", * normal = "\e[0m";

int main (int argc, char * argv[])
{
    Trend::CMS = "#scale[1.5]{#bf{CMS}}"; 

    if (argc < 4) {
        cerr << argv[0] << " firstRun lastRun lines.json\n";
        return EXIT_FAILURE;
    }

    const Run2Lumi GetLumi("deliveredLumi.txt", atoi(argv[1]), atoi(argv[2]));

    fs::path json = argv[3];
    assert(fs::exists(json));
    pt::ptree lines;
    pt::read_json(json.c_str(), lines);

    vector<tuple<const char *, const char *, const char *>> VSs {
        {"g_xResol200", "#scale[0.6]{#sum} p_{T} (vertex) > 200 GeV", "Primary-Vertex x-Resolution  [#mum]"},
        {"g_zResol400", "#scale[0.6]{#sum} p_{T} (vertex) > 400 GeV", "Primary-Vertex z-Resolution  [#mum]"}
    };

    fs::path pname = "SplitVertexTrends.root";
    cout << pname << endl;
    assert(fs::exists(pname));
    auto f = TFile::Open(pname.c_str());
    f->ls();

    for (auto VS: VSs) {

        auto name = get<0>(VS),
             title = get<1>(VS),
             ytitle = get<2>(VS);

        cout << bold << name << normal << endl;

        float ymin = 0, ymax = 30;
        Trend trend(name, title, ytitle, ymin, ymax, lines, GetLumi);
        trend.lgd.SetHeader(title);

        static const vector<tuple<const char *, Color_t, Style_t>> graphs {
            { "Alignment during data-taking" , kBlue    , kFullSquare      },
            { "End-of-year re-reconstruction", kRed     , kFullCircle      },
            { "Legacy reprocessing"          , kGreen+2 , kFullTriangleDown}
        };
        for (auto graph: graphs) {
            TString gtitle = get<0>(graph);
            cout << gtitle << endl;
            auto g = Get<TGraph>("%s_%s", name, get<0>(graph));
            assert(g != nullptr);
            g->SetMarkerSize(0.6);
            g->SetMarkerColor(get<1>(graph));
            g->SetMarkerStyle(get<2>(graph));
            g->SetTitle(""); // for the legend
            bool fullRange = gtitle.Contains("data-taking") || gtitle.Contains("Legacy");
            if (!fullRange) {
                // 320606 last IOV of APEs EOY...
                // 321831 next pixel template

                cout << "Removing:";
                for (int n = g->GetN()-1; n >= 0 && g->GetPointX(n) >= 320606; --n) {
                    cout << ' ' << g->GetPointX(n);
                    g->RemovePoint(n);
                }
                cout << endl;
            }
            trend(g, "PZ", "p", fullRange);

            // dirty trick to get bigger marker in the legend 
            double x[] = {-99};
            auto g2 = new TGraph(1,x,x);
            g2->SetMarkerColor(get<1>(graph));
            g2->SetMarkerStyle(get<2>(graph));
            g2->SetTitle(gtitle); // for the legend

            trend(g2, "P2", "p", false);
        }
    }

    f->Close();
    cout << bold << "Done" << normal << endl;
    return EXIT_SUCCESS;
}


