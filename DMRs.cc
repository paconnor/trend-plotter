#include <cstdlib>

#include <iostream>
#include <tuple>

#include <TFile.h>
#include <TGraphErrors.h>
#include <TH1.h>
#include <TString.h>

#include "Trend.h"

using namespace std;

static const char * bold = "\e[1m", * normal = "\e[0m";

int main (int argc, char * argv[])
{
    Trend::CMS = "#scale[1.5]{#bf{CMS}}"; 

    if (argc < 4) {
        cerr << argv[0] << " firstRun lastRun lines.json\n";
        return EXIT_FAILURE;
    }

    const Run2Lumi GetLumi("deliveredLumi.txt", atoi(argv[1]), atoi(argv[2]));

    fs::path json = argv[3];
    assert(fs::exists(json));
    pt::ptree lines;
    pt::read_json(json.c_str(), lines);

    fs::path pname = "DMRtrends_rootFile_18May.root";
    cout << pname << endl;
    assert(fs::exists(pname));
    auto f = TFile::Open(pname.c_str());

    vector<tuple<const char *, const char *, float, float>> DMRs {
        {"musigma", "#mu  [#mum]", -6, 6},
        {"deltamusigmadeltamu", "#Delta#mu  [#mum]", -15, 15}
    };

    vector<tuple<const char *, Color_t, Style_t>> alignments {
        { "Alignment during data-taking" , kBlue    , kFullSquare      },
        { "End-of-year re-reconstruction", kRed     , kFullCircle      },
        { "Legacy reprocessing"          , kGreen+2 , kFullTriangleDown}
    };

    for (auto DMR: DMRs) {

        auto name  = get<0>(DMR),
             ytitle = get<1>(DMR);

        cout << bold << name << normal << endl;

        float ymin = get<2>(DMR), ymax = get<3>(DMR);
        Trend trend(name, ytitle, ytitle, ymin, ymax, lines, GetLumi);
        trend.lgd.SetHeader("BPIX (x')");

        for (auto alignment: alignments) {
            auto graph = get<0>(alignment);
            cout << graph << endl;
            TString gname = Form("median_%s_BPIX_%s", graph, name);
            gname.ReplaceAll(" ", "_");
            gname.ReplaceAll("data-taking", "data_taking");
            cout << gname << endl;
            auto g = Get<TGraphErrors>(gname);
            assert(g != nullptr);
            g->SetTitle("");
            g->SetMarkerColor(get<1>(alignment));
            g->SetFillColorAlpha(get<1>(alignment),0.2); // note: I had to hardcode the alpha also in the Trend class...
            g->SetLineColor(kWhite);
            g->SetMarkerStyle(get<2>(alignment));
            g->SetMarkerSize(0.6);
            bool fullRange = gname.Contains("data-taking") || gname.Contains("Legacy");
            trend(g, "P2", "pf", fullRange);

            // dirty trick to get bigger marker in the legend 
            double x[] = {-99};
            auto g2 = new TGraph(1,x,x);
            g2->SetTitle(graph);
            g2->SetMarkerColor(get<1>(alignment));
            g2->SetFillColorAlpha(get<1>(alignment),0.2); // note: I had to hardcode the alpha also in the Trend class...
            g2->SetLineColor(kWhite);
            g2->SetMarkerStyle(get<2>(alignment));
            trend(g2, "P2", "pf", false);
        }
    }

    f->Close();
    cout << bold << "Done" << normal << endl;
    return EXIT_SUCCESS;
}


