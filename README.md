# Generic trend plotting macro

## Code

`Trend.*` is the implementation of the generic plotting macro.
It currently converts a `TGraphErrors` given as a function of the run number into another `TGraphErrors` as a function of the luminosity.
(The implementation to convert a `TH1` is drafted but not yet ready.)
The macro might need some fine tuning, but on the long term could be used for the trend plotting in the new all-in-one tool nearly as is.

`APEs` is an example of how the plotting macro can be used.
It should be used for the UL paper and as an example for other implementation.

## Input files

 - `lines.json` is an example of configuration file for the vertical lines and for the labels in the trend.  It should also be nearly ready.
 - `lumi.txt` is taken from the official CMSSW repo.
 - The APE files may be downloaded from [Marius' CERNbox](https://cernbox.cern.ch/index.php/s/qmJ37y7A1D2UG1m).
 - The vertex-split file can be found at `/afs/cern.ch/user/a/avagneri/public/SplitVertexTrends.root`
 - The PV file can be found at `/afs/cern.ch/cms/CAF/CMSALCA/ALCA_TRACKERALIGN/data/commonValidation/alignmentObjects/tkello/CMSSW_10_6_0/src/Alignment/OfflineValidation_old/test/PV_trends_paper_runNBaxis/PVtrends.root`
 - The DMR file can be found at `/eos/cms/store/group/alca_trackeralign/TRK_20_001/DMRtrends/DMRtrends_rootFile_18May.root`.
 - The barycentre file may be downloaded from [Tongguang's CERNbox](`https://cernbox.cern.ch/index.php/s/VmuyINmCD0a61LS`).

## Running

Compile with `make -j3`.
Run with `make run`.

## Lumi file

Run BrilCalc per year to produce unfiltered (only BRIL normTag filter but no JSON) stable lumidata in csv format
(change option -u if needed)

```
brilcalc lumi --amodetag PROTPHYS -u /fb --normtag /cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_BRIL.json --begin "01/01/16 00:00:00" --end "12/31/16 23:59:59" --output-style csv > stable2016.csv
brilcalc lumi --amodetag PROTPHYS -u /fb --normtag /cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_BRIL.json --begin "01/01/17 00:00:00" --end "12/31/17 23:59:59" --output-style csv > stable2017.csv
brilcalc lumi --amodetag PROTPHYS -u /fb --normtag /cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_BRIL.json --begin "01/01/18 00:00:00" --end "12/31/18 23:59:59" --output-style csv > stable2018.csv
```

Merge and change into suitable format (requires Python3)
```
python skimLumiFile.py -i stable2016.csv,stable2017.csv,stable2018.csv -o lumiperFullRun2_deliveredNoFilter.csv --lumiType delivered

Options: 
  -h, --help            show this help message and exit
  -i INPUTFILES, --inputFiles INPUTFILES
                        Comma separated list of input CSV lumifiles.
  -o OUTPUTFILE, --outputFile OUTPUTFILE
                        Skimmed output lumifile name [".csv",".txt",".json"].
  --lumiType LUMITYPE   Define lumi values to export ["delivered","recorded"].
```
