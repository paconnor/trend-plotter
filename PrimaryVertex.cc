#include <cstdlib>

#include <iostream>
#include <tuple>

#include <TFile.h>
#include <TGraph.h>
#include <TH1.h>
#include <TString.h>

#include "Trend.h"

using namespace std;

static const char * bold = "\e[1m", * normal = "\e[0m";

int main (int argc, char * argv[])
{
    Trend::CMS = "#scale[1.5]{#bf{CMS}}"; 

    if (argc < 4) {
        cerr << argv[0] << " firstRun lastRun lines.json\n";
        return EXIT_FAILURE;
    }

    const Run2Lumi GetLumi("deliveredLumi.txt", atoi(argv[1]), atoi(argv[2]));

    fs::path json = argv[3];
    assert(fs::exists(json));
    pt::ptree lines;
    pt::read_json(json.c_str(), lines);

    vector<tuple<const char *, Color_t, Style_t>> alignments {
        { "Alignment during data-taking" , kBlue    , kFullSquare      },
        { "End-of-year re-reconstruction", kRed     , kFullCircle      },
        { "Legacy reprocessing"          , kGreen+2 , kFullTriangleDown}
    };

    fs::path pname = "PVtrends.root";
    cout << pname << endl;
    assert(fs::exists(pname));
    auto f = TFile::Open(pname.c_str());

    {
        Trend mean("PV_mean", "mean of impact parameter in transverse plane",
                      "#LT d_{xy} #GT  [#mum]", -7., 10., lines, GetLumi),
              RMS ("PV_RMS" , "RMS of impact parameter in transverse plane as a function of pseudorapidity", 
                      "RMS of d_{xy}(#eta)  [#mum]", 0., 35., lines, GetLumi);
        mean.lgd.SetHeader("p_{T} (track) > 3 GeV");
        RMS .lgd.SetHeader("p_{T} (track) > 3 GeV");

        for (auto alignment: alignments) {
            TString gname = get<0>(alignment);
            cout << gname << endl;
            gname.ReplaceAll(" ", "_");

            auto gMean = Get<TGraph>("mean_%s_dxy_eta_vs_run", gname.Data());
            auto hRMS  = Get<TH1   >( "RMS_%s_dxy_eta_vs_run", gname.Data());
            assert(gMean != nullptr);
            assert(hRMS  != nullptr);

            TString gtitle = get<0>(alignment);
            gMean->SetTitle(""); // for the legend
            gMean->SetMarkerSize(0.6);
            gMean->SetMarkerColor(get<1>(alignment));
            gMean->SetMarkerStyle(get<2>(alignment));
            hRMS ->SetTitle(""); // for the legend
            hRMS ->SetMarkerSize(0.6);
            hRMS ->SetMarkerColor(get<1>(alignment));
            hRMS ->SetMarkerStyle(get<2>(alignment));

            bool fullRange = gname.Contains("data-taking") || gname.Contains("Legacy");
            mean(gMean, "PZ", "p", fullRange);
            RMS (hRMS , ""  , "p", fullRange);

            // dirty trick to get larger marker in the legend
            double x[] = {-99};
            auto g2 = new TGraph(1,x,x);
            g2->SetTitle(gtitle); // for the legend
            g2->SetMarkerColor(get<1>(alignment));
            g2->SetMarkerStyle(get<2>(alignment));
            mean(g2, "PZ", "p", false);
            RMS (g2, "PZ", "p", false);
        }
    }

    f->Close();
    cout << bold << "Done" << normal << endl;
    return EXIT_SUCCESS;
}


