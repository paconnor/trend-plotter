#include <cstdlib>

#include <iostream>

#include <TFile.h>
#include <TGraph.h>
#include <TString.h>

#include "Trend.h"

using namespace std;

static const char * bold = "\e[1m", * normal = "\e[0m";

int main (int argc, char * argv[])
{
    Trend::CMS = "#scale[1.5]{#bf{CMS}}"; 

    if (argc < 4) {
        cerr << argv[0] << " firstRun lastRun lines.json\n";
        return EXIT_FAILURE;
    }

    const Run2Lumi GetLumi("deliveredLumi.txt", atoi(argv[1]), atoi(argv[2]));

    fs::path json = argv[3];
    assert(fs::exists(json));
    pt::ptree lines;
    pt::read_json(json.c_str(), lines);

    map<const char *, const char *> APUs {
        {"trend_Y_BpixLayer1In_byRun", "APUs of 1st BPIX layer inner sensors"},
        {"trend_Y_BpixLayer2In_byRun", "APUs of 2nd BPIX layer inner sensors"}
    };

    for (auto APU: APUs) {

        auto name  = APU.first,
             title = APU.second;

        cout << bold << name << normal << endl;

        fs::path pname = name;
        pname += ".root";

        cout << pname << endl;
        assert(fs::exists(pname));

        auto f = TFile::Open(pname.c_str()); // in the present case, we take the trends from a root file, but this does not necessarily have to be the case in general

        float ymin = 0, ymax = 100;
        Trend trend(name, title, "#sigma_{align,y}  [#mum]", ymin, ymax, lines, GetLumi);
        trend.lgd.SetHeader(title);

        static const vector<TString> graphs { "Alignment during data-taking",
                                "End-of-year re-reconstruction", "Legacy reprocessing"};
        for (auto graph: graphs) {
            cout << graph << endl;
            auto g = Get<TGraph>(graph);
            g->SetTitle(graph); // for the legend
            bool fullRange = graph.Contains("data-taking") || graph.Contains("Legacy");
            trend(g, "PZ", "l", fullRange);
        }
        f->Close();
    }

    cout << bold << "Done" << normal << endl;
    return EXIT_SUCCESS;
}
