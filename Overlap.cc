#include <cstdlib>

#include <iostream>
#include <tuple>

#include <TFile.h>
#include <TGraphErrors.h>
#include <TH1.h>
#include <TString.h>

#include "Trend.h"

using namespace std;

static const char * bold = "\e[1m", * normal = "\e[0m";

int main (int argc, char * argv[])
{
    Trend::CMS = "#scale[1.5]{#bf{CMS}}"; 

    if (argc < 4) {
        cerr << argv[0] << " firstRun lastRun lines.json\n";
        return EXIT_FAILURE;
    }

    const Run2Lumi GetLumi("deliveredLumi.txt", atoi(argv[1]), atoi(argv[2]));

    fs::path json = argv[3];
    assert(fs::exists(json));
    pt::ptree lines;
    pt::read_json(json.c_str(), lines);

    vector<tuple<const char *, const char *, Color_t, Style_t>> alignments {
        {"Prompt", "Alignment during data-taking" , kBlue    , kFullSquare      },
        {"ReReco", "End-of-year re-reconstruction", kRed     , kFullCircle      },
        {"UL"    , "Legacy reprocessing"          , kGreen+2 , kFullTriangleDown}
    };

    vector<tuple<const char *, const char *, float, float>> overlaps {
        {"Z", "z", -120, 120},
        {"Phi", "#phi", -7, 12}
    };

    for (auto overlap: overlaps) {

        auto X = get<0>(overlap), x = get<1>(overlap);
        auto ymin = get<2>(overlap), ymax = get<3>(overlap);

        auto name = Form("%s_%s_BPIX", X, X),
             title = Form("Overlap along %s coordinate of modules neighbour w.r.t. %s", x, x),
             ytitle = Form("#scale[1.3]{#LT #delta_{%s} #GT_{%s}}  [#mum]", x, x);

        Trend trend(name, title, ytitle, ymin, ymax, lines, GetLumi);
        trend.lgd.SetHeader("BPIX");

        for (auto& alignment: alignments) {

            fs::path fn = Form("%s_%s.root", get<0>(alignment), name);
            assert(fs::exists(fn));
            auto f = TFile::Open(fn.c_str());

            bool earlyStop = TString(get<0>(alignment)) == "ReReco";

            auto g = Get<TGraph>("Graph");
            //g->SetTitle(get<1>(alignment));
            g->SetTitle("");
            g->SetFillColorAlpha(get<2>(alignment),0.2); // note: I had to hardcode the alpha also in the Trend class...
            g->SetLineColor(kWhite);
            g->SetMarkerColor(get<2>(alignment));
            g->SetMarkerStyle(get<3>(alignment));
            g->SetMarkerSize(0.6);

            trend(g, "P2", "pf", !earlyStop);

            // dirty trick to get bigger marker in the legend 
            double x[] = {-99};
            auto g2 = new TGraph(1,x,x);
            g2->SetTitle(get<1>(alignment));
            g2->SetFillColorAlpha(get<2>(alignment),0.2); // note: I had to hardcode the alpha also in the Trend class...
            g2->SetLineColor(kWhite);
            g2->SetMarkerColor(get<2>(alignment));
            g2->SetMarkerStyle(get<3>(alignment));

            trend(g2, "P2", "pf", false);

            f->Close();
        }
    }

    cout << bold << "Done" << normal << endl;
    return EXIT_SUCCESS;
}


